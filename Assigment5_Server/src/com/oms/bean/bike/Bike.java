package com.oms.bean.bike;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.oms.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("bike")
@JsonSubTypes({ @Type(value = NormalBike.class, name = "normalbike"), @Type(value = EBike.class, name = "ebike") })
public abstract class Bike {
	protected String id;
	protected String typeId;
	protected String parkingId;
	protected String name;
	protected float weight;
	protected String licensePlate;
	protected String manuafacturingDate;
	protected float cost;
	protected String producer;
	
	public Bike() {
		super();
	}
	public Bike(
		String id,
		String typeId,
		String parkingId,
		String name,
		float weight,
		String licensePlate,
		String manuafacturingDate,
		float cost,
		String producer
	){
		this.id = id;
		this.typeId = typeId;
		this.parkingId = parkingId;
		this.name = name;
		this.weight = weight;
		this.cost = cost;
		this.producer = producer;
		this.manuafacturingDate = manuafacturingDate;
		this.licensePlate = licensePlate;
	}

	public void cloneFromOtherBike(Bike bike) {
		if (bike instanceof Bike) {
			id = bike.id;
			typeId = bike.typeId;
			parkingId = bike.parkingId;
			name = bike.name;
			weight = bike.weight;
			cost = bike.cost;
			producer = bike.producer;
			manuafacturingDate = bike.manuafacturingDate;
			licensePlate = bike.licensePlate;
		}
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getParkingId() {
		return parkingId;
	}
	public void setParkingId(String parkingId) {
		this.parkingId = parkingId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public String getManuafacturingDate() {
		return manuafacturingDate;
	}
	public void setManuafacturingDate(String manuafacturingDate) {
		this.manuafacturingDate = manuafacturingDate;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public String getProducer() {
		return producer;
	}
	public void setProducer(String producer) {
		this.producer = producer;
	}
	
	@Override
	public String toString() {
		return
			"id: " + this.id +
			"typeId: " + this.typeId +
			"parkingId: " + this.parkingId +
			"name: " + this.name +
			"weight: " + this.weight +
			"licensePlate: " + this.licensePlate +
			"manuafacturingDate: " + this.manuafacturingDate +
			"cost: " + this.cost +
			"producer: " + this.producer;
	}
	public boolean match(Bike bike) {
		if (bike == null)
			return true;
		
		if (bike.id != null && !bike.id.equals("") && !this.id.contains(bike.id)) {
			return false;
		}
		if (bike.typeId != null && !bike.typeId.equals("") && !this.typeId.contains(bike.typeId)) {
			return false;
		}
		if (bike.parkingId != null && !bike.parkingId.equals("") && !this.parkingId.contains(bike.parkingId)) {
			return false;
		}
		if (bike.name != null && !bike.name.equals("") && !this.name.contains(bike.name)) {
			return false;
		}
		if (bike.producer != null && !bike.producer.equals("") && !this.producer.contains(bike.producer)) {
			return false;
		}
		if (bike.manuafacturingDate != null && !bike.manuafacturingDate.equals("") && !this.manuafacturingDate.contains(bike.manuafacturingDate)) {
			return false;
		}
		if (bike.licensePlate != null && !bike.licensePlate.equals("") && !this.licensePlate.contains(bike.licensePlate)) {
			return false;
		}
		if (bike.cost != 0 && !(this.cost == bike.cost)) {
			return false;
		}
		if (bike.weight != 0 && !(this.weight == bike.weight)) {
			return false;
		}
		
		return true;
	}
		
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bike) {
			return this.id.equals(((Bike) obj).id);
		}
		return false;
	}
	
	public boolean checkBikeById(String id) {
		return this.id.equals(id);
	}
	
	public boolean checkBikeByParkingId(String parkingId) {
		return this.parkingId.equals(parkingId);
	}
	
	public boolean isValidIntance() {
		if (this.id == null || this.id.trim() == "") return false;
		if (this.parkingId == null) return false;
		if (this.name == null || this.id.trim() == "") return false;
		if (this.producer == null || this.producer.trim() == "") return false;
		if (this.manuafacturingDate == null
				|| this.manuafacturingDate.trim() == ""
				|| !DateUtil.isValidDateYYYYMMDD(this.manuafacturingDate, false)) return false;
		if (this.licensePlate == null || this.licensePlate.trim() == "") return false;
		if (this.cost < 0) return false;
		if (this.weight <= 0) return false;
		return true;
		
	}
}