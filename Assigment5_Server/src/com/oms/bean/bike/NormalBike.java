package com.oms.bean.bike;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class NormalBike extends Bike {
	public NormalBike() {
		super();
	}

	public NormalBike(
			String id,
			String typeId,
			String parkingId,
			String name,
			float weight,
			String licensePlate,
			String manuafacturingDate,
			float cost,
			String producer
	){
		super(
			id,
			typeId,
			parkingId,
			name,
			weight,
			licensePlate,
			manuafacturingDate,
			cost,
			producer
		);
	}

	@Override
	public boolean match(Bike bike) {
		if (bike == null)
			return true;
		
		
		boolean res = super.match(bike);
		if (!res) {
			return false;
		}
				
		if (!(bike instanceof NormalBike))
			return false;
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof NormalBike) {
			return this.id.equals(((NormalBike) obj).id);
		}
		return false;
	}
}
