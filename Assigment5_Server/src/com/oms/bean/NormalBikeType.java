package com.oms.bean;



public class NormalBikeType extends BikeType {
	
	public NormalBikeType() {
		super();
	}
	
	public NormalBikeType(String id, String name, int so_yen, int so_ghe_sau, float muc_gia, int tien_coc) {
		super(id, name, so_yen, so_ghe_sau, muc_gia, tien_coc);
	}

	
	@Override
	public String toString() {
		return super.toString();
	}
	
	@Override
	public boolean match(BikeType bike) {
		if (bike == null)
			return true;
		
		
		boolean res = super.match(bike);
		if (!res) {
			return false;
		}
		
		
		if (!(bike instanceof NormalBikeType))
			return false;

		return true;
	}
}