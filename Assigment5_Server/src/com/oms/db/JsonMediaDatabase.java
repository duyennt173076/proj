package com.oms.db;

import java.util.ArrayList;

import com.oms.bean.BikeType;
import com.oms.bean.Card;
import com.oms.bean.Cost;
import com.oms.bean.Parking;
import com.oms.bean.RentalData;
import com.oms.db.seed.Seed;

public class JsonMediaDatabase implements IMediaDatabase{
	private static IMediaDatabase singleton = new JsonMediaDatabase();
	private ArrayList<Parking> parkings = Seed.singleton().getParking();
	private ArrayList<BikeType> biketypes = Seed.singleton().getBikeTypes();
	private ArrayList<Card> cards = Seed.singleton().getCards();
	
	//
	private ArrayList<Cost> costs = Seed.singleton().getCost();
	
	//
	private JsonMediaDatabase() {
	}
	
	public static IMediaDatabase singleton() {
		return singleton;
	}

	@Override
	public ArrayList<Parking> searchParking(Parking parking) {
		ArrayList<Parking> res = new ArrayList<Parking>();
		for (Parking b: parkings) {
			if (b.match(parking)) {
				res.add(b);
			}
		}
		return res;
	}

	@Override
	public Parking addParking(Parking parking) {
		for (Parking m: parkings) {
			if (m.equals(parking)) {
				return null;
			}
		}
		
		parkings.add(parking);
		return parking;
	}
	
	@Override
	public Parking updateParking(Parking parking) {
		for (Parking m: parkings) {
			if (m.equals(parking)) {
				parkings.remove(m);
				parkings.add(parking);
				return parking;
			}
		}
		return null;
	}
	@Override
	public Parking removeParking(Parking parking) {
		for (Parking m: parkings) {
			if (m.equals(parking)) {
				parkings.remove(m);
				return parking;
			}
		}
		return null;
	}
	
	
	@Override
	public ArrayList<BikeType> searchBikeType(BikeType biketype) {
		ArrayList<BikeType> res = new ArrayList<BikeType>();
		for (BikeType b: biketypes) {
			if (b.match(biketype)) {
				res.add(b);
			}
		}
		return res;
	}

	@Override
	public BikeType addBikeType(BikeType biketype) {
		if (biketypes ==null) biketypes = new ArrayList<BikeType>();
		for (BikeType m: biketypes) {
			if (m.equals(biketype)) {
				return null;
			}
		}
		
		biketypes.add(biketype);
		return biketype;
	}
	
	@Override
	public BikeType removeBikeType(BikeType biketype) {
		
		for (BikeType m: biketypes) {
			if (m.equals(biketype)) {
				biketypes.remove(m);
				return biketype;
			}
		}
		
		return null;
	}
	
	@Override
	public BikeType updateBikeType(BikeType biketype) {
		for (BikeType m: biketypes) {
			if (m.equals(biketype)) {
				biketypes.remove(m);
				biketypes.add(biketype);
				return biketype;
			}
		}
		return null;
	}
	@Override
	public BikeType getInforFromID(String id) {
		for (BikeType m: biketypes) {
			if (m.getId().equals(id)) {
				return m;
			}
		}
		return null;
	}
	
	@Override
	public boolean matchBikeType(BikeType biketype) {
		for (BikeType b: biketypes) {
			if (b.match(biketype)) {
//				Logger.printConsole(biketype);
				return true;
			}
		}
		return false;
	}

	@Override
	public ArrayList<RentalData> searchBikeById(RentalData rentalData) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public ArrayList<Card> searchCard(Card card) {
		ArrayList<Card> res = new ArrayList<Card>();
		for (Card b: cards) {
			if (b.match(card)) {
				res.add(b);
			}
		}
		return res;
	}

	@Override
	public Card addCard(Card card) {
		if (cards==null) cards = new ArrayList<Card>();

			for (Card m: cards) {
				if (m.equals(card)) {
					return null;
				}
			}
		cards.add(card);
		return card;
	}
	
	@Override
	public Card updateCard(Card card) {
		for (Card m: cards) {
			if (m.equals(card)) {
				cards.remove(m);
				cards.add(card);
				return card;
			}
		}
		return null;
	}

	@Override
	public Card deleteCard(Card card) {
		for (Card m: cards) {
			if (m.equals(card)) {
				cards.remove(m);
				return card;
			}
		}
		return null;
	}
	
	//
	@Override
	public ArrayList<Cost> searchCost(Cost cost) {
		ArrayList<Cost> res = new ArrayList<Cost>();
		for (Cost b: costs) {
			if (b.match(cost)) {
				res.add(b);
			}
		}
		return res;
	}

	@Override
	public Cost addCost(Cost cost) {
		for (Cost m: costs) {
			if (m.equals(cost)) {
				return null;
			}
		}
		
		costs.add(cost);
		return cost;
	}
	
	@Override
	public Cost updateCost(Cost cost) {
		for (Cost m: costs) {
			if (m.equals(cost)) {
				costs.remove(m);
				costs.add(cost);
				return cost;
			}
		}
		return null;
	}

	@Override
	public Cost deleteCost(Cost cost) {
		for (Cost m: costs) {
			if (m.equals(cost)) {
				costs.remove(m);
				return cost;
			}
		}
		return null;
	}
}
