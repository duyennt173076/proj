package com.oms.db.seed.bike;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oms.bean.bike.Bike;
import com.oms.config.BikeConfig;
import com.oms.db.seed.FileReader;
import com.oms.util.log.Logger;

public class BikesData {
	private static String nameFileJsonBike = "./normalbike.json";
	private static String nameFileJsonEBike = "./ebike.json";
	private ArrayList<Bike> bikes;
	
	private static BikesData singleton = new BikesData();
	
	private BikesData() {
		start();
	}
	
	public static BikesData singleton() {
		return singleton;
	}
	
	private void start() {
		bikes = new ArrayList<Bike>();
		bikes.addAll(generateDataFromFile( new File(getClass().getResource(nameFileJsonBike).getPath()).toString()));
		bikes.addAll(generateDataFromFile( new File(getClass().getResource(nameFileJsonEBike).getPath()).toString()));
		
		System.out.println("BikeData:: Load bike data from json success!");
		if (BikeConfig.enableLog) {
			Logger.printConsole(bikes);
		}
	}
	
	private ArrayList<? extends Bike> generateDataFromFile(String filePath){
		ArrayList<? extends Bike> res = new ArrayList<Bike>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Bike>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("BikeData:: Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	public void writeValueFromObj(ArrayList<Bike> bikes) {
		ObjectMapper mapper = new ObjectMapper();
		System.out.println("BikeData:: writeValueFromObj");
		try {
			mapper.writeValue(new File("log/bikeUpdate.json"), bikes);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("BikeData:: Invalid convert bike data to JSON");
		}
	}
	
	public ArrayList<Bike> getAllBikes() {
		return bikes;
	}


	public static void main(String[] args) {
		new BikesData();
		System.out.println("OK");
	}
}
