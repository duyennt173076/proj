package com.oms.db.bike.helper;

import com.oms.bean.bike.Bike;

public interface IGetBikeByIdDatabase {
	public Bike getBikeById(String id);
}
