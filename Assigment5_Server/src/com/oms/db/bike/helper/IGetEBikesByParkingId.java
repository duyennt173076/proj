package com.oms.db.bike.helper;

import java.util.ArrayList;

import com.oms.bean.bike.EBike;

public interface IGetEBikesByParkingId {
	public ArrayList<EBike> getEBikesByParkingId(String parkingId);
}
