package com.oms.db.bike.helper;

import java.util.ArrayList;

import com.oms.bean.bike.NormalBike;

public interface IGetNormalBikesByParkingId {
	public ArrayList<NormalBike> getNormalBikesByParkingId(String parkingId);
}
