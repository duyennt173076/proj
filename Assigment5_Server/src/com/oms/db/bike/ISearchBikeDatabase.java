package com.oms.db.bike;

import java.util.ArrayList;

import com.oms.bean.BikeType;
import com.oms.bean.bike.Bike;
import com.oms.playload.bike.BikePayload;

public interface ISearchBikeDatabase {
	public ArrayList<? extends BikePayload> searchBike(Bike bike, BikeType bikeType);
}
