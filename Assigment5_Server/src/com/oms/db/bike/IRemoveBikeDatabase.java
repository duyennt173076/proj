package com.oms.db.bike;

import com.oms.playload.bike.BikePayload;

public interface IRemoveBikeDatabase {
	public BikePayload removeBike(String id);
}
