package com.oms.db.bike;

import com.oms.bean.bike.Bike;
import com.oms.playload.bike.BikePayload;

public interface IAddBikeDatabase {
	public BikePayload addBike(Bike bike);
}