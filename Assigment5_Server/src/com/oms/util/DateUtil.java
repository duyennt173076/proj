package com.oms.util;

import java.util.Calendar;
import java.util.Date;

import com.oms.util.log.Logger;

public class DateUtil {
	// valid if from year 1 ->
	public static boolean isValidDateYYYYMMDD(String s, boolean checkLessNowDate) { // ex "2020/02/28"
		int count = 0;
		for (int i = 0; i < s.length(); i++){
		    char c = s.charAt(i);        
		    count += (c == '/') ? 1 : 0;
		}
		if (count != 2) return false;
		String[] all = s.split("/");
		if (all.length == 3) {
			if (all[0].trim() == "" || all[0].length() > 4) return false;
			if (all[1].trim() == "" || all[1].length() > 2) return false;
			if (all[2].trim() == "" || all[2].length() > 2) return false;
			Integer y = StringUtil.getInt(all[0]);
			if (y == null || y < 1) return false;
			Integer m = StringUtil.getInt(all[1]);
			if (m == null) return false;
			Integer d = StringUtil.getInt(all[2]);
			if (d == null) return false;
			@SuppressWarnings("deprecation")
			Date date = new Date(y, m, d);
			if (checkLessNowDate) {
				boolean isValidD = isValidDate(date);
				if (!isValidD) return false;
//				Logger.printConsole(new Date().);
				Logger.printConsole(date);
				return new Date().after(date);
			}
			return isValidDate(date);
		}
		return false;
	}
	public static boolean isValidDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setLenient(false);
		cal.setTime(date);
		try {
		    cal.getTime();
		}
		catch (Exception e) {
		  System.out.println("Invalid date");
		  return false;
		}
		return true;
	}
	public static void main (String[] s) {
		System.out.print(isValidDateYYYYMMDD("2020/12/20", true));
	}
}
