package com.oms.playload.bike;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.oms.bean.BikeType;
import com.oms.bean.bike.Bike;

@JsonSerialize
public class NormalBikePayload extends BikePayload{

	public NormalBikePayload(Bike bike, BikeType bikeType) {
		// TODO Auto-generated constructor stub
		super(bike, bikeType);
	}
}
