package com.oms.playload.bike;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.oms.bean.BikeType;
import com.oms.bean.bike.Bike;
import com.oms.bean.bike.EBike;

public class EBikePayload extends BikePayload {
	@JsonSerialize
	protected float batteryPercentage;
	@JsonSerialize
	protected int loadCycles;
	@JsonSerialize
	protected float estimatedUsageTimeRemaining;
	
	public EBikePayload() {
	}
	public EBikePayload(
		Bike bike,
		BikeType bikeType
	) {
		super(bike, bikeType);
		batteryPercentage = ((EBike) bike).getBatteryPercentage();
		loadCycles = ((EBike) bike).getLoadCycles();
		estimatedUsageTimeRemaining = ((EBike) bike).getEstimatedUsageTimeRemaining();
	}
}
