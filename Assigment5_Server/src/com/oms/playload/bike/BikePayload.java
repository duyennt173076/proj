package com.oms.playload.bike;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.oms.bean.BikeType;
import com.oms.bean.bike.Bike;

public class BikePayload {
	@JsonSerialize
	protected String id;
	@JsonSerialize
	protected String typeId;
	@JsonSerialize
	protected String parkingId;
	@JsonSerialize
	protected String name;
	@JsonSerialize
	protected float weight;
	@JsonSerialize
	protected String licensePlate;
	@JsonSerialize
	protected String manuafacturingDate;
	@JsonSerialize
	protected float cost;
	@JsonSerialize
	protected String producer;
//	float batteryPercentage;
//	int loadCycles;
//	float estimatedUsageTimeRemaining;
	@JsonSerialize
	protected String nameType;
	@JsonSerialize
	protected int so_yen;
//	@JsonSerialize
//	protected int tien_coc;
	@JsonSerialize
	protected int so_ghe_sau;
	@JsonSerialize
	protected int deposit;
	
	public BikePayload() {
	}
	public BikePayload(
		Bike bike,
		BikeType bikeType
	) {
		super();
		id = bike.getId();
		typeId = bike.getTypeId();
		parkingId = bike.getParkingId();
		name = bike.getName();
		weight = bike.getWeight();
		cost = bike.getCost();
		producer = bike.getProducer();
		manuafacturingDate = bike.getManuafacturingDate();
		licensePlate = bike.getLicensePlate();
		nameType = bikeType.getName();
		so_yen = bikeType.getSo_yen();
		so_ghe_sau = bikeType.getSo_ghe_sau();
		deposit = bikeType.getTien_coc();
	}
}
