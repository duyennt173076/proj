package com.oms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.oms.bean.BikeType;
import com.oms.bean.RentalData;
import com.oms.bean.bike.Bike;
import com.oms.db.IMediaDatabase;
import com.oms.db.JsonMediaDatabase;
import com.oms.db.seed.Seed;
import com.oms.db.seed.bike.BikesData;

@Path("/bike")
public class RentalService {

	private IMediaDatabase mediaDatabase;

	public RentalService() {
		mediaDatabase = JsonMediaDatabase.singleton();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<RentalData> getBike(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("address") String address, @QueryParam("distance") float distance,
			@QueryParam("time") float time) {
		ArrayList<Bike> bikes = BikesData.singleton().getAllBikes();
		ArrayList<RentalData> rs = new ArrayList<RentalData>();
		
		for (Bike b: bikes) {
			if (b.getId().equals(id)&&!b.getParkingId().equals("")) {
				RentalData r = new RentalData();
				r.setId(id);
				r.setName(b.getName());
				r.setTypeId(b.getTypeId());
				r.setParkingId(b.getParkingId());
				r.setWeight(b.getWeight());
				r.setLicensePlate(b.getLicensePlate());
				r.setManuafacturingDate(b.getManuafacturingDate());
				r.setCost(b.getCost());
				r.setProducer(b.getProducer());
				
				List<BikeType> bts = Seed.singleton().getBikeTypes();
				for (BikeType bt: bts) {
					if (b.getTypeId().equals(bt.getId())) {
						r.setNameType(bt.getName());
						r.setSo_yen(bt.getSo_yen());
						r.setTien_coc(bt.getTien_coc());
						r.setSo_ghe_sau(bt.getSo_ghe_sau());
						break;
					}
				}
				rs.add(r);
				break;
			}
		}
		return rs;
	}
	
	@POST
	@Path("/{id}/{CardId}")
	@Produces(MediaType.APPLICATION_JSON)
	public void updateBike(@PathParam("id") String id, @PathParam("CardId") String CardId) {
		ArrayList<Bike> bikes = BikesData.singleton().getAllBikes();
		for (Bike b: bikes) {
			if (b.getId().equals(id)) {
				b.setParkingId("");
				Map<String, Map<Date, Bike>> map = Seed.singleton().getRentalDatas();
				Map<Date, Bike> rentalDate = new HashMap<Date, Bike>();
				rentalDate.put(new Date(), b);
				map.put(CardId, rentalDate);
				break;
			}
		}
	}
	
//	@POST
//	@Path("/{id}")
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
//	public RentalData updateBike(@PathParam("id") String id, RentalData bike) {
//		return mediaDatabase.updateBike(bike);
//	}
}