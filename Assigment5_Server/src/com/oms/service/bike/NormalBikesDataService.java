package com.oms.service.bike;
import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.oms.bean.BikeType;
import com.oms.bean.NormalBikeType;
import com.oms.bean.bike.Bike;
import com.oms.bean.bike.NormalBike;
import com.oms.db.bike.IBikeInListDatabase;
import com.oms.db.bike.JsonBikeInListDatabase;
import com.oms.playload.bike.BikePayload;
import com.oms.playload.bike.NormalBikePayload;
import com.oms.util.log.Logger;

@Path("/normalbike")
public class NormalBikesDataService {

	private IBikeInListDatabase bikesDatabase;

	public NormalBikesDataService() {
		bikesDatabase = JsonBikeInListDatabase.singleton();
	}

	@SuppressWarnings("unchecked")
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<NormalBikePayload> getBike(
		@QueryParam("id") String id,
		@QueryParam("typeId") String typeId,
		@QueryParam("parkingId") String parkingId,
		@QueryParam("name") String name,
		@QueryParam("weight") float weight,
		@QueryParam("licensePlate") String licensePlate,
		@QueryParam("manuafacturingDate") String manuafacturingDate,
		@QueryParam("producer") String producer,
		@QueryParam("cost") float cost,
		@QueryParam("nameType") String nameBikeType,
		@QueryParam("so_yen") int countSaddles,
		@QueryParam("so_ghe_sau") int countSeats
	) {
		NormalBike b = new NormalBike(
			id,
			typeId,
			parkingId,
			name,
			weight,
			licensePlate,
			manuafacturingDate,
			cost,
			producer
		);
		BikeType bikeType = new NormalBikeType(
			"",
			nameBikeType,
			countSaddles,
			countSeats,
			0,
			0
		);
//		Logger.printConsole(bikeType);
		ArrayList<? extends BikePayload> resSearch = bikesDatabase.searchBike(b, bikeType);
		ArrayList<NormalBikePayload> res = new ArrayList<NormalBikePayload>();
		try {
			if (resSearch != null) res = (ArrayList<NormalBikePayload>) resSearch; //TODO ArrayList<Bike> res = bikesDatabase.searchBike(b, bikeType)
		}
		catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERROR: cast in public ArrayList<NormalBikePayload> getBike(");
		}
		return res;
	}
	

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BikePayload updateBike(
		@PathParam("id") String id,
		NormalBike bike
	) {
		return bikesDatabase.updateBike(bike);
	}
	
	@POST
	@Path("/remove")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BikePayload removeBike(
		NormalBike bike
	) {
		return bikesDatabase.removeBike(bike.getId());
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BikePayload addBike(
		NormalBike bike
	) {
		return bikesDatabase.addBike(bike);
	}
}