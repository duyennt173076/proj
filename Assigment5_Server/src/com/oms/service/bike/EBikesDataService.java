package com.oms.service.bike;
import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.oms.bean.BikeType;
import com.oms.bean.EBikeType;
import com.oms.bean.NormalBikeType;
import com.oms.bean.bike.Bike;
import com.oms.bean.bike.EBike;
import com.oms.db.bike.IBikeInListDatabase;
import com.oms.db.bike.JsonBikeInListDatabase;
import com.oms.playload.bike.BikePayload;
import com.oms.playload.bike.EBikePayload;
import com.oms.playload.bike.NormalBikePayload;
import com.oms.util.log.Logger;

@Path("/ebike")
public class EBikesDataService {

	private IBikeInListDatabase bikesDatabase;

	public EBikesDataService() {
		bikesDatabase = JsonBikeInListDatabase.singleton();
	}

	@SuppressWarnings("unchecked")
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EBikePayload> getBike(
		@QueryParam("id") String id,
		@QueryParam("typeId") String typeId,
		@QueryParam("parkingId") String parkingId,
		@QueryParam("name") String name,
		@QueryParam("weight") float weight,
		@QueryParam("licensePlate") String licensePlate,
		@QueryParam("manuafacturingDate") String manuafacturingDate,
		@QueryParam("producer") String producer,
		@QueryParam("cost") float cost,
		@QueryParam("batteryPercentage") float batteryPercentage,
		@QueryParam("loadCycles") int loadCycles,
		@QueryParam("estimatedUsageTimeRemaining") float estimatedUsageTimeRemaining,
		@QueryParam("nameType") String nameBikeType,
		@QueryParam("so_yen") int countSaddles,
		@QueryParam("so_ghe_sau") int countSeats
	) {
		EBike b = new EBike(
			id,
			typeId,
			parkingId,
			name,
			weight,
			licensePlate,
			manuafacturingDate,
			cost,
			producer,
			batteryPercentage,
			loadCycles,
			estimatedUsageTimeRemaining
		);
		BikeType bikeType = new EBikeType(
			"",
			nameBikeType,
			countSaddles,
			countSeats,
			0,
			0
		);
		ArrayList<? extends BikePayload> resSearch = bikesDatabase.searchBike(b, bikeType);
		ArrayList<EBikePayload> res = null;
		try {
			if (resSearch != null) res = (ArrayList<EBikePayload>) resSearch; //TODO ArrayList<Bike> res = bikesDatabase.searchBike(b, bikeType)
		}
		catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERROR: cast in public ArrayList<EBikePayload> getBike(");
		}
		return res;
	}
	

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BikePayload updateBike(
		@PathParam("id") String id,
		EBike bike
	) {
//		Logger.printObjectToJsonFile("bikeUpdate", bike);
		return bikesDatabase.updateBike(bike);
	}
	
	@POST
	@Path("/remove")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BikePayload removeBike(
		EBike bike
	) {
		return bikesDatabase.removeBike(bike.getId());
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BikePayload addBike(
		EBike bike
	) {
		return bikesDatabase.addBike(bike);
	}
}