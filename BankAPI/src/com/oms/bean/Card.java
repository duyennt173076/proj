package com.oms.bean;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("card")
@JsonSubTypes({ @Type(value = Card.class, name = "card") })
public class Card {
	private String id;
	private String name;
	private int money;
	private String securityCode;
	private String exprirationDate;
	
	public Card() {
		super();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getExprirationDate() {
		return exprirationDate;
	}

	public void setExprirationDate(String expirationDate) {
		this.exprirationDate = expirationDate;
	}

	public Card(String id, String name, int money, String securityCode, String expirationDate) {
		super();
		this.id = id;
		this.name = name;
		this.money = money;
		this.securityCode = securityCode;
		this.exprirationDate = expirationDate;
	}



	public Card(String id, String name,
			String securityCode, String expirationDate) {
		super();
		this.id = id;
		this.name = name;
		this.securityCode = securityCode;
		this.exprirationDate = expirationDate;
	}

	public Card(String id) {
		super();
		this.id = id;
	}

	@Override
	public String toString() {
		return "Card [id=" + id + ", name=" + name + ", money=" + money + ", securityCode=" + securityCode
				+ ", expirationDate=" + exprirationDate + "]";
	}
	
	public boolean match(Card card) {
		if (card == null)
			return true;
		if (!(card instanceof Card))
			return false;
		if (card.id != null && !card.id.equals("") && !this.id.contains(card.id)) {
			return false;
		}
		if (card.name != null && !card.name.equals("") && !this.name.contains(card.name)) {
			return false;
		}
		if (card.money != 0 && this.money != card.money) {
			return false;
		}
		if (card.securityCode != null && !card.securityCode.equals("") && !this.securityCode.contains(card.securityCode)) {
			return false;
		}
		if (card.exprirationDate != null && !card.exprirationDate.equals("") && !this.exprirationDate.contains(card.exprirationDate)) {
			return false;
		}
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Card) {
			return this.id.equals(((Card) obj).id);
		}
		return false;
	}
}