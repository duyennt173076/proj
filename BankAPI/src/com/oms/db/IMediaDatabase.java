package com.oms.db;

import java.util.ArrayList;

import com.oms.bean.Card;

public interface IMediaDatabase {
	public ArrayList<Card> getCard(Card card);
	public Card addCard(Card card);
	public Card updateCard(Card card);
	public Card getCardbyId(Card card);
	public Card match(Card card);
}
