package com.oms.db;

import java.util.ArrayList;
import java.util.Comparator;

import com.oms.bean.Card;
import com.oms.db.seed.Seed;

public class JsonMediaDatabase implements IMediaDatabase {
	private static IMediaDatabase singleton = new JsonMediaDatabase();

	private ArrayList<Card> cards = Seed.singleton()
			.getCard();
	
	private JsonMediaDatabase() {
		
	}

	public static IMediaDatabase singleton() {
		return singleton;
	}

	@Override
	public ArrayList<Card> getCard(Card card) {
		cards.sort(new Comparator<Card>() {
			@Override
			public int compare(Card o1, Card o2) {
				if (o1.getId() == o2.getId()) {
					return 0;
				}
				if (o1.getId() == null) {
					return -1;
				}
				if (o2.getId() == null) {
					return 1;
				}
				return o1.getId()
						.compareTo(o2.getId());
			}
		});
		ArrayList<Card> res = new ArrayList<Card>();
		for (Card c : cards) {
			if (c.match(card)) {
				res.add(c);
//				break;
			}
		}
		return res;
	}
	
	@Override
	public Card getCardbyId(Card card) {
		cards.sort(new Comparator<Card>() {
			@Override
			public int compare(Card o1, Card o2) {
				if (o1.getId() == o2.getId()) {
					return 0;
				}
				if (o1.getId() == null) {
					return -1;
				}
				if (o2.getId() == null) {
					return 1;
				}
				return o1.getId()
						.compareTo(o2.getId());
			}
		});
		Card res = null;
		for (Card c : cards) {
			if (c.match(card)) {
				res = c;
				break;
			}
		}
		return res;
	}

	@Override
	public Card addCard(Card card) {
		for (Card c : cards) {
			if (c.equals(card)) {
				return null;
			}
		}
		cards.add(card);
		return card;
	}

	@Override
	public Card updateCard(Card card) {
		for (Card c : cards) {
			if (c.equals(card)) {
				cards.remove(c);
				cards.add(card);
				return card;
			}
		}
		return null;
	}

	@Override
	public Card match(Card card) {
		for (Card c : cards) {
			if (c.equals(card)) {
				if (!c.getExprirationDate().equals(card.getExprirationDate()))
					return null;
				if (!c.getName().equals(card.getName()))
					return null;
				if (!c.getSecurityCode().equals(card.getSecurityCode()))
					return null;
				return c;
			}
		}
		return null;
	}

}
