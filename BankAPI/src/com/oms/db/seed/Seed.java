package com.oms.db.seed;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oms.bean.Card;

public class Seed {
	private ArrayList<Card> cards;
	
	private static Seed singleton = new Seed();
	
	private Seed() {
		start();
	}
	
	public static Seed singleton() {
		return singleton;
	}
	
	private void start() {
		cards = new ArrayList<Card>();
		cards.addAll(generateDataFromFile( new File(getClass().getResource("./cards.json").getPath()).toString()));
	}
	
	private ArrayList<? extends Card> generateDataFromFile(String filePath){
		ArrayList<? extends Card> res = new ArrayList<Card>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Card>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		return res;
	}
	
	public ArrayList<Card> getCard() {
		return cards;
	}

	public static void main(String[] args) {
		Seed seed = new Seed();
		seed.start();
	}

	
}
