package com.oms.serverapi;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.oms.bean.bike.Bike;
import com.oms.bean.bike.BikePayload;
import com.oms.bean.bike.EBike;
import com.oms.bean.bike.EBikePayload;
import com.oms.bean.bike.NormalBike;
import com.oms.bean.bike.NormalBikePayload;
import com.oms.util.log.Logger;

public class BikeApi {
	private static final String PATH = "http://localhost:8080/";
	public static final String PATH_NORMALBIKE = "normalbike";
	public static final String PATH_EBIKE = "ebike";
	private static final String PATH_REMOVE = "remove";
	private static final String PATH_ADD = "add";

	private static BikeApi singleton = new BikeApi();
	private Client client;
	
	public BikeApi() {
		client = ClientBuilder.newClient();
	}
	
	public static BikeApi singleton() {
		return singleton;
	}

	public ArrayList<? extends Bike> getBikes(Map<String, String> queryParams, String path) {
		WebTarget webTarget = client.target(PATH).path(path);
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<? extends Bike> res = null;
		switch(path) {
			case PATH_NORMALBIKE:
//				System.out.println("Nresponse.readEntity(String.class)");
//				InputStream responseBody = (InputStream) response.getEntity();
//				Scanner s = new Scanner(responseBody).useDelimiter("\\A");
//				String result = s.hasNext() ? s.next() : "";
//				System.out.println(result);

				res = response.readEntity(new GenericType<ArrayList<NormalBike>>() {});
				break;
			case PATH_EBIKE:
//				System.out.println("Eresponse.readEntity(String.class)");
//				System.out.println(response.readEntity(String.class));
				res = response.readEntity(new GenericType<ArrayList<EBike>>() {});
				break;
		}
		System.out.println(res);
		return res;
	}
	
	public Bike updateBike(Bike bike) {
		String path;
		if (bike instanceof NormalBike) path = PATH_NORMALBIKE;
		else if (bike instanceof EBike) path = PATH_EBIKE;
		else return null;
				
		WebTarget webTarget = client.target(PATH).path(path).path(bike.getId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response;
		Bike res = null;
		switch(path) {
			case PATH_NORMALBIKE:
				NormalBikePayload l = (NormalBikePayload) new NormalBikePayload().cloneFromBike(bike);
//				Logger.printConsole(l);
				response = invocationBuilder.post(Entity.entity(l, MediaType.APPLICATION_JSON));
				res = response.readEntity(NormalBike.class);
				break;
			case PATH_EBIKE:
				EBikePayload e = (EBikePayload) new EBikePayload().cloneFromBike(bike);
//				Logger.printConsole(e);
				response = invocationBuilder.post(Entity.entity(e, MediaType.APPLICATION_JSON));
				res = response.readEntity(EBike.class);
				break;
		}
		System.out.println(res);
		return res;
	}
	
	public Bike addBike(Bike bike) {
		String path;
		if (bike instanceof NormalBike) path = PATH_NORMALBIKE;
		else if (bike instanceof EBike) path = PATH_EBIKE;
		else return null;
				
		WebTarget webTarget = client.target(PATH).path(path).path(PATH_ADD);
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = null;
		
		Bike res = null;
		switch(path) {
			case PATH_NORMALBIKE:
//				Logger.printConsole(new NormalBikePayload().cloneFromBike(bike));
				response = invocationBuilder.post(Entity.entity(new NormalBikePayload().cloneFromBike(bike), MediaType.APPLICATION_JSON));
				res = response.readEntity(NormalBike.class);
				break;
			case PATH_EBIKE:
//				Logger.printConsole(new EBikePayload().cloneFromBike(bike));
				response = invocationBuilder.post(Entity.entity(new EBikePayload().cloneFromBike(bike), MediaType.APPLICATION_JSON));
				res = response.readEntity(EBike.class);
				break;
		}
		System.out.println(res);
		return res;
	}
	
	public Bike removeBike(Bike bike) {
		String path;
		if (bike instanceof NormalBike) path = PATH_NORMALBIKE;
		else if (bike instanceof EBike) path = PATH_EBIKE;
		else return null;
		
		WebTarget webTarget = client.target(PATH).path(path).path(PATH_REMOVE);
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = null;
		
		Bike res = null;
		switch(path) {
			case PATH_NORMALBIKE:
				response = invocationBuilder.post(Entity.entity(new NormalBikePayload().cloneFromBike(bike), MediaType.APPLICATION_JSON));
				res = response.readEntity(NormalBike.class);
				break;
			case PATH_EBIKE:
				response = invocationBuilder.post(Entity.entity(new EBikePayload().cloneFromBike(bike), MediaType.APPLICATION_JSON));
				res = response.readEntity(EBike.class);
				break;
		}
		System.out.println(res);
		return res;
	}
	
	public static void main(String[] args) {
//		BikeApi.singleton().getBikes(null, PATH_EBIKE);
//		BikeApi.singleton().addBike( new EBike(
//			"ebike10",
//			"ebike",
//			"parking99",
//			"Osama",
//			26.1f,
//			"99999",
//			"20202",
//			60000.5f,
//			"HoangHaMobile",
//			60.9f,
//			3,
//			6.9f
//		));

//		Bike bike = new EBike(
//	        "ebike1997",
//	        "ebike1",
//	        "parking1",
//	        "Asama 2020",
//	        1.8f,
//	        "2020574",
//	        "20201112",
//	        260000.0f,
//	        "eVIN",
//	        100.0f,
//	        2,
//	        3.0f,
//	        "0",
//	        1,
//	        1,
//	        1
//		);
		
//		Logger.printConsole(bike);
//		BikeApi.singleton().removeBike(bike);
//		BikeApi.singleton().removeBike(bike);
//		bike.setParkingId("");
//		BikeApi.singleton().updateBike(bike);
	}
}
