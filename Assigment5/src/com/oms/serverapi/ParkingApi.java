package com.oms.serverapi;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.oms.bean.Parking;


public class ParkingApi {
	public static final String PATH = "http://localhost:8080/";
	
	private Client client;
	
	public ParkingApi() {
		client = ClientBuilder.newClient();
	}
	
	

	public ArrayList<Parking> getParking(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("parking");
		
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Parking> res = response.readEntity(new GenericType<ArrayList<Parking>>() {});
		System.out.println(res);
		return res;
	}
	

	public Parking updateParking(Parking parking) {
		WebTarget webTarget = client.target(PATH).path("parking").path(parking.getId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(parking, MediaType.APPLICATION_JSON));
		
		Parking res = response.readEntity(Parking.class);
		return res;
	}
	//
	public Parking addParking(Parking parking) {
		WebTarget webTarget = client.target(PATH).path("parking").path("add");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(parking, MediaType.APPLICATION_JSON));
		
		Parking res = response.readEntity(Parking.class);
		return res;
	}



	public Parking removeParking(Parking parking) {
		WebTarget webTarget = client.target(PATH).path("parking").path("delete");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(parking, MediaType.APPLICATION_JSON));
		
//		Parking res = response.readEntity(Parking.class);
		return null;
	}
	
	
	
	
	
}
