package com.oms.serverapi;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.oms.bean.Card;


public class CardApi {
	public static final String PATH = "http://localhost:8080/";
	
	private Client client;
	
	public CardApi() {
		client = ClientBuilder.newClient();
	}
	
	public ArrayList<Card> getCard(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("card");
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Card> res = response.readEntity(new GenericType<ArrayList<Card>>() {});
		System.out.println(res);
		return res;
	}
	

	public Card updateCard(Card card) {
		WebTarget webTarget = client.target(PATH).path("card").path(card.getId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(card, MediaType.APPLICATION_JSON));
		
		Card res = response.readEntity(Card.class);
		return res;
	}



	public Card addCard(Card card) {
		WebTarget webTarget = client.target(PATH).path("card").path("add");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(card, MediaType.APPLICATION_JSON));
		
		Card res = response.readEntity(Card.class);
		return res;
	}



	public Card deleteCard(Card card) {
		WebTarget webTarget = client.target(PATH).path("card").path("delete");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(card, MediaType.APPLICATION_JSON));
		
//		Card res = response.readEntity(Card.class);
		return null;
	}
	
	
	
	
	
}
