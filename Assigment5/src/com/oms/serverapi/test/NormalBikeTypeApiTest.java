package com.oms.serverapi.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.oms.bean.NormalBikeType;
import com.oms.bean.BikeType;
import com.oms.serverapi.BikeTypeApi;


public class NormalBikeTypeApiTest {
	private BikeTypeApi api = new BikeTypeApi();
	@Test
	public void testGetAllBikeType() {
		ArrayList<BikeType> list= api.getAllBikeTypes();
		assertEquals("Error in getAllBikeType API!", list.size(), 3);
	}
	@Test
	public void testGetAllEBikeType() {
		ArrayList<NormalBikeType> list= api.getNormalBikeTypes(null);
		assertEquals("Error in getAllNormalBikeType API!", list.size(), 2);
	}
	
	@Test(timeout = 1000)
	public void testResponse() {
		api.getAllBikeTypes();
	}
	
	@Test
	public void testUpdateEBikeType() {
		ArrayList<NormalBikeType> list= api.getNormalBikeTypes(null);
		assertTrue("No data", list.size() > 0);
		
		
		NormalBikeType ebiketype= list.get(0);
		String newName= "xe dap don thuong 2";
		ebiketype.setName(newName);
		api.updateNormalBikeType(ebiketype);
		
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("name", newName);
		list = api.getNormalBikeTypes(params);
		assertTrue("Eror in updateEBikeType API!", list.size() > 0);
		
	
		NormalBikeType newEBikeType = api.getNormalBikeTypes(params).get(0);
		assertEquals("Eror in updateEBikeType API!", newEBikeType.getName(), newName);
	}

}
