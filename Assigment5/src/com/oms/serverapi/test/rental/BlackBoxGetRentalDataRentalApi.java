package com.oms.serverapi.test.rental;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.oms.bean.RentalData;
import com.oms.serverapi.RentalApi;

public class BlackBoxGetRentalDataRentalApi {
	
	private RentalApi api = new RentalApi();
	
	/**
	 * Thuê xe bike1
	 */
	static {
		RentalData r = new RentalData();
		r.setId("bike1");
		new RentalApi().rentalBike(r, "card1");
	}

	/**
	 * Truyền queryParams null: Không trả về kết quả nào
	 */
	@Test
	public void testGetBike1() {
		ArrayList<RentalData> list= api.getRentalData(null);
		assertEquals("Error in getRentalData API!", list.size(), 0);
	}
	
	/**
	 * Truyền queryParams có trường mã xe (id) trống: Không trả về kết quả nào
	 */
	@Test
	public void testGetBike2() {
		Map<String, String> queryPrams = new HashMap<String, String>();
		queryPrams.put("id", "");
		ArrayList<RentalData> list= api.getRentalData(queryPrams);
		assertEquals("Error in getRentalData API!", list.size(), 0);
	}
	
	/**
	 * Truyền queryParams có trường mã xe (id) = null: Không trả về kết quả nào
	 */
	@Test
	public void testGetBike3() {
		Map<String, String> queryPrams = new HashMap<String, String>();
		queryPrams.put("id", null);
		ArrayList<RentalData> list= api.getRentalData(queryPrams);
		assertEquals("Error in getRentalData API!", list.size(), 0);
	}

	/**
	 * Truyền queryParams có trường mã xe (id) trống: Không trả về kết quả nào
	 */
	@Test
	public void testGetBike4() {
		Map<String, String> queryPrams = new HashMap<String, String>();
		queryPrams.put("id", "");
		ArrayList<RentalData> list= api.getRentalData(queryPrams);
		assertEquals("Error in getRentalData API!", list.size(), 0);
	}

	/**
	 * Truyền queryParams có trường mã xe (id) không giống với mã xe nào trong database: 
	 * Không trả về kết quả nào
	 */
	@Test
	public void testGetBike5() {
		Map<String, String> queryPrams = new HashMap<String, String>();
		queryPrams.put("id", "car1");
		ArrayList<RentalData> list= api.getRentalData(queryPrams);
		assertEquals("Error in getRentalData API!", list.size(), 0);
	}

	/**
	 * Truyền queryParams 2 trường khác id: Không trả về kết quả nào
	 */
	@Test
	public void testGetBike6() {
		Map<String, String> queryPrams = new HashMap<String, String>();
		queryPrams.put("title", "");
		queryPrams.put("name", "");
		ArrayList<RentalData> list= api.getRentalData(queryPrams);
		assertEquals("Error in getRentalData API!", list.size(), 0);
	}

	/**
	 * Truyền queryParams có trường mã xe (id) đúng nhưng trả về nhiều hơn 1 kết quả: 
	 * Trả về 1 kết quả
	 * có id đúng với id xe
	 */
	@Test
	public void testGetBike7() {
		Map<String, String> queryPrams = new HashMap<String, String>();
		queryPrams.put("id", "bike2");
		ArrayList<RentalData> list= api.getRentalData(queryPrams);
		assertEquals("Error in getRentalData API!", list.size(), 1);
		assertEquals("Error in getRentalData API!", list.get(0).getId(), "bike2");
	}
	
	/**
	 * Truyền queryParams có trường mã xe (id) đúng nhưng xe đã thuê:
	 * Không trả về kết quả nào
	 */
	@Test
	public void testGetBike8() {
		Map<String, String> queryPrams = new HashMap<String, String>();
		queryPrams.put("id", "bike1");
		ArrayList<RentalData> list= api.getRentalData(queryPrams);
		assertEquals("Error in getRentalData API!", list.size(), 0);
	}
	
	
	/**
	 * Nhập đúng mã xe: trả về 1 xe có id giống với id param
	 */
	@Test
	public void testGetBike9() {
		Map<String, String> queryPrams = new HashMap<String, String>();
		queryPrams.put("id", "bike0");
		ArrayList<RentalData> list= api.getRentalData(queryPrams);
		assertEquals("Error in getRentalData API!", list.size(), 1);
		assertEquals("Error in getRentalData API!", list.get(0).getId(), "bike0");
	}

	/**
	 * Truyền queryParams có trường mã xe (id) và 1 trường khác: Không trả về kết quả nào
	 */
	@Test
	public void testGetBike10() {
		Map<String, String> queryPrams = new HashMap<String, String>();
		queryPrams.put("id", "");
		queryPrams.put("name", "");
		ArrayList<RentalData> list= api.getRentalData(queryPrams);
		assertEquals("Error in getRentalData API!", list.size(), 0);
	}
}
