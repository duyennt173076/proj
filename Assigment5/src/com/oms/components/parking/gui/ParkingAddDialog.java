package com.oms.components.parking.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.Parking;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataAddDialog;

@SuppressWarnings("serial")
public class ParkingAddDialog extends ADataAddDialog<Parking> {

	private JTextField idField;
	private JTextField nameField;
	private JTextField addressField;
	private JTextField distanceField;
	private JTextField timeField;
	private JTextField numberOfBikesField;
	private JTextField numberOfeBikesField;
	private JTextField numberOfTwinBikesField;
	private JTextField numberOfEmtyDocksField;

	public ParkingAddDialog(IDataManageController<Parking> controller) {
		super(controller);
	}

	@Override
	public void buildControls() {
		t = new Parking();
		int row = getLastRowIndex();
		JLabel idLabel = new JLabel("Id");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(idLabel, c);
		idField = new JTextField(15);

		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(idField, c);

		row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Name");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);

		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);

		row = getLastRowIndex();
		JLabel addressLabel = new JLabel("Address");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(addressLabel, c);
		addressField = new JTextField(15);

		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(addressField, c);

		row = getLastRowIndex();
		JLabel distanceLabel = new JLabel("Distance");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(distanceLabel, c);
		distanceField = new JTextField(15);

		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(distanceField, c);

		row = getLastRowIndex();
		JLabel timeLabel = new JLabel("Time");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(timeLabel, c);
		timeField = new JTextField(15);

		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(timeField, c);

		row = getLastRowIndex();
		JLabel numberOfBikesLabel = new JLabel("number Of Bikes");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numberOfBikesLabel, c);
		numberOfBikesField = new JTextField(15);

		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numberOfBikesField, c);

		row = getLastRowIndex();
		JLabel numberOfeBikesLabel = new JLabel("number Of eBikes");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numberOfeBikesLabel, c);
		numberOfeBikesField = new JTextField(15);

		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numberOfeBikesField, c);

		row = getLastRowIndex();
		JLabel numberOfTwinBikesLabel = new JLabel("number Of TwinBikes");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numberOfTwinBikesLabel, c);
		numberOfTwinBikesField = new JTextField(15);

		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numberOfTwinBikesField, c);

		row = getLastRowIndex();
		JLabel numberOfEmtyDocksLabel = new JLabel("number Of EmtyDocks");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numberOfEmtyDocksLabel, c);
		numberOfEmtyDocksField = new JTextField(15);

		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numberOfEmtyDocksField, c);

	}

	@Override
	public Parking getNewData() {
		t.setId(idField.getText());
		t.setName(nameField.getText());
		t.setAddress(addressField.getText());
		t.setDistance(Float.parseFloat(distanceField.getText()));
		t.setTime(Float.parseFloat(timeField.getText()));
		t.setNumberOfBikes(Integer.parseInt(numberOfBikesField.getText()));
		t.setNumberOfeBikes(Integer.parseInt(numberOfeBikesField.getText()));
		t.setNumberOfTwinBikes(Integer.parseInt(numberOfTwinBikesField.getText()));
		t.setNumberOfEmtyDocks(Integer.parseInt(numberOfEmtyDocksField.getText()));
		return t;
	}
}
