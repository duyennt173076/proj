package com.oms.components.parking.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.oms.bean.Parking;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.abstractdata.gui.ADataSinglePane;
import com.oms.components.parking.controller.AdminParkingPageController;



@SuppressWarnings("serial")

public class AdminParkingListPane extends ADataListPane<Parking>{
	
	public AdminParkingListPane(ADataPageController<Parking> controller) {
		this.controller = controller;
	}
	
	@Override
	public void decorateSinglePane(ADataSinglePane<Parking> singlePane) {
		JButton editbutton = new JButton("Edit");
		singlePane.addDataHandlingComponent(editbutton);
		JButton deleteButton = new JButton("delete");
		singlePane.addDataHandlingComponent(deleteButton);
		JButton button2 = new JButton("Add");
		singlePane.addDataHandlingComponent(button2);
		
		IDataManageController<Parking> manageController = new IDataManageController<Parking>() {
			@Override
			public void update(Parking t) {
				if (controller instanceof AdminParkingPageController) {
					Parking newParking = ((AdminParkingPageController) controller).updateParking(t);
					singlePane.updateData(newParking);
				}
			}

			@Override
			public void create(Parking t) {
			}

			@Override
			public void read(Parking t) {
			}

			@Override
			public void delete(Parking t) {
				if (controller instanceof AdminParkingPageController) {
					((AdminParkingPageController) controller).removeParking(t);
					singlePane.setVisible(false);
				}
				
			}
		};
		
		editbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new ParkingEditDialog(singlePane.getData(), manageController);
			}
		});
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				manageController.delete(singlePane.getData());
			}
		});	
	}
}

