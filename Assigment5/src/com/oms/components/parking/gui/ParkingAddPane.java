package com.oms.components.parking.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;

import com.oms.bean.Parking;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataAddPane;
import com.oms.components.parking.controller.AdminParkingPageController;
import com.sun.xml.bind.v2.schemagen.xmlschema.List;

@SuppressWarnings("serial")
public class ParkingAddPane extends ADataAddPane<Parking> {
	
	public ParkingAddPane(ADataPageController<Parking> controller) {
		this.setController(controller);
	}

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		JButton addButton = new JButton("Add");
		add(addButton, c);
		
		IDataManageController<Parking> manageController = new IDataManageController<Parking>() {
			@Override
			public void update(Parking t) {
			}

			@Override
			public void create(Parking t) {
				if (controller instanceof AdminParkingPageController) {
					((AdminParkingPageController) controller).addParking(t);
					appendListPane(t);
				}
			}

			@Override
			public void read(Parking t) {
			}

			@Override
			public void delete(Parking t) {
				
			}
		};
		
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new ParkingAddDialog(manageController);
			}
		});	
	}
	
}

