package com.oms.components.bike.ebike;

import javax.swing.JTextField;

import com.oms.bean.bike.Bike;
import com.oms.bean.bike.EBike;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.bike.gui.BikeEditDialog;
import com.oms.util.JTextFieldUtil;

@SuppressWarnings("serial")
public class EBikeEditDialog extends BikeEditDialog{
	
	private JTextField batteryPercentage;
	private JTextField loadCycles;
	private JTextField estimatedUsageTimeRemaining;
	
	public EBikeEditDialog(Bike bike, IDataManageController<Bike> controller) {
		super(bike, controller);
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
		if (t instanceof EBike) {
			EBike bike = (EBike) t;
			
			batteryPercentage = addSlot("Battery Percentage", Float.toString(bike.getBatteryPercentage()));
			loadCycles = addSlot("Load Cycles", Integer.toString(bike.getLoadCycles()));
			estimatedUsageTimeRemaining = addSlot("Estimated Usage Time Remaining", Float.toString(bike.getEstimatedUsageTimeRemaining()));
		}
	}
	
	@Override
	public Bike getNewData() {
		super.getNewData();
		if (t instanceof EBike) {
			EBike bike = (EBike) t;
			
			bike.setBatteryPercentage(JTextFieldUtil.getFloatIfSuccess(batteryPercentage, bike.getBatteryPercentage()));
			bike.setLoadCycles(JTextFieldUtil.getIntegerIfSuccess(loadCycles, bike.getLoadCycles()));
			bike.setEstimatedUsageTimeRemaining(JTextFieldUtil.getFloatIfSuccess(estimatedUsageTimeRemaining, bike.getEstimatedUsageTimeRemaining()));
		}
		
		return t;
	}
}