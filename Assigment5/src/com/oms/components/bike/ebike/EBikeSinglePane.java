package com.oms.components.bike.ebike;

import javax.swing.JLabel;

import com.oms.bean.bike.EBike;
import com.oms.components.bike.gui.BikeSinglePane;

@SuppressWarnings("serial")
public class EBikeSinglePane extends BikeSinglePane {
	private JLabel batteryPercentage;
	private JLabel loadCycles;
	private JLabel estimatedUsageTimeRemaining;
	
	public EBikeSinglePane() {
		super();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();
		
		batteryPercentage = addSlot();
		loadCycles = addSlot();
		estimatedUsageTimeRemaining = addSlot();
	}
	
	@Override
	public void displayData() {
		super.displayData();
		
		if (t instanceof EBike) {
			EBike bike = (EBike) t;
			batteryPercentage.setText("Battery Percentage: " + bike.getBatteryPercentage());
			loadCycles.setText("Load Cycles: " + bike.getLoadCycles());
			estimatedUsageTimeRemaining.setText("Estimated Usage Time Remaining: " + bike.getEstimatedUsageTimeRemaining());
		}
	}
}
