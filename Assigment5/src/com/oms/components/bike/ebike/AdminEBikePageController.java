package com.oms.components.bike.ebike;

import java.util.List;
import java.util.Map;

import com.oms.bean.bike.Bike;
import com.oms.components.bike.controller.AdminBikePageController;
import com.oms.serverapi.BikeApi;

public class AdminEBikePageController extends AdminBikePageController {
	@Override
	public EBikeSinglePane createSinglePane() {
		return new EBikeSinglePane();
	}
	
	@Override
	public List<? extends Bike> search(Map<String, String> searchParams) {
		return BikeApi.singleton().getBikes(searchParams, BikeApi.PATH_EBIKE);
	}
}
