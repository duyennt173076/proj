package com.oms.components.bike.controller;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import com.oms.bean.bike.Bike;
import com.oms.bean.bike.EBike;
import com.oms.bean.bike.NormalBike;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.controller.IDataCreateController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.abstractdata.gui.ADataPagePane;
import com.oms.components.abstractdata.gui.ADataSearchPane;
import com.oms.components.abstractdata.gui.ADataSinglePane;
import com.oms.components.bike.BikeState;
import com.oms.components.bike.ebike.EBikeCreateDialog;
import com.oms.components.bike.gui.AdminBikeListPane;
import com.oms.components.bike.gui.BikeCreateDialog;
import com.oms.components.bike.gui.BikeSearchPane;
import com.oms.components.bike.gui.BikeSinglePane;
import com.oms.serverapi.BikeApi;

public class AdminBikePageController extends ADataPageController<Bike> implements IDataCreateController<Bike> {
	public AdminBikePageController() {
		super();
		
//		@SuppressWarnings("unchecked")
//		ADataPagePane<Bike> page = (ADataPagePane<Bike>) this.getDataPagePane();
//		JPanel addPane = this.getAddPane();
//		SpringLayout layout = (SpringLayout) page.getLayout();
//		layout.putConstraint(SpringLayout.EAST, addPane, 605, SpringLayout.NORTH, page);
//		layout.putConstraint(SpringLayout.NORTH, addPane, -20, SpringLayout.SOUTH, page);
//		page.add(addPane);
	}
	
	@Override
	public ADataListPane<Bike> createListPane() {
		return new AdminBikeListPane(this);
	}
	
	public Bike updateBike(Bike bike) {
		return BikeApi.singleton().updateBike(bike);
	};
	
	public Bike removeBike(Bike bike) {
		return BikeApi.singleton().removeBike(bike);
	}
	
	@Override
	public Bike create(Bike bike) {
		return BikeApi.singleton().addBike(bike);
	}

	@Override
	public ADataSearchPane createSearchPane() {
		return new BikeSearchPane();
	}

	@Override
	public List<? extends Bike> search(Map<String, String> searchParams) {
		return BikeApi.singleton().getBikes(searchParams, BikeApi.PATH_NORMALBIKE);
	}

	@Override
	public ADataSinglePane<Bike> createSinglePane() {
		return new BikeSinglePane();
	}
	
//	public JPanel getAddPane() {
//		JPanel panel = new JPanel();
//        panel.setLayout(new BorderLayout());
////        panel.setLayout(new FlowLayout(FlowLayout.RIGHT));
//
//        JButton button = new JButton("Add");
//        panel.add(button);
//        
//        AdminBikePageController controller = this;
//        
//        button.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				switch (BikeState.getInstance().getStateTab()) {
//				case BikeState.NORMALBIKE:
//					new BikeCreateDialog(new NormalBike(), controller);
//					break;
//				case BikeState.EBIKE:
//					new EBikeCreateDialog(new EBike(), controller);
//					break;
//				}
//			}
//		});	
//        return panel;
//	}
	
}