package com.oms.components.bike;

public class BikeState {
	public static final int NORMALBIKE = 0;
	public static final int EBIKE = 1;
	public static final int ADD = 0;
	public static final int EDIT = 1;
	private int stateTab = NORMALBIKE;
	private int stateDialog = EDIT;
	
	private static BikeState _instance = new BikeState();
	public static BikeState getInstance() {
		return _instance;
	}
	
	public int getStateTab() {
		return stateTab;
	}
	public void setStateTab(int stateTab) {
		this.stateTab = stateTab;
	}

	public int getStateDialog() {
		return stateDialog;
	}

	public void setStateDialog(int stateDialog) {
		this.stateDialog = stateDialog;
	}
}
