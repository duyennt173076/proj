package com.oms.components.bike.gui;

import com.oms.bean.bike.Bike;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.abstractdata.gui.ADataSinglePane;

@SuppressWarnings("serial")

public class UserSearchBikeListPane extends ADataListPane<Bike>{
	
	public UserSearchBikeListPane(ADataPageController<Bike> controller) {
		this.controller = controller;
	}
	
	@Override
	public void decorateSinglePane(ADataSinglePane<Bike> singlePane) {
	}
}

