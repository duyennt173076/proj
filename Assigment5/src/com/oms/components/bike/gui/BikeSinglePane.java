package com.oms.components.bike.gui;

import javax.swing.JLabel;

import com.oms.bean.bike.Bike;
import com.oms.components.abstractdata.gui.ADataSinglePane;

@SuppressWarnings("serial")
public class BikeSinglePane extends ADataSinglePane<Bike>{
	private JLabel id;
	private JLabel typeId;
	private JLabel parkingId;
	private JLabel name;
	private JLabel weight;
	private JLabel licensePlate;
	private JLabel manuafacturingDate;
	private JLabel cost;
	private JLabel producer;
	private JLabel nameType;
	private JLabel so_yen;
//	private JLabel so_ban_dap;
	private JLabel so_ghe_sau;
	private JLabel deposit;

	public BikeSinglePane() {
		super();
	}
	
	public BikeSinglePane(Bike bike) {
		this();
		this.t = bike;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
		id = addSlot();
		typeId = addSlot();
		parkingId = addSlot();
		name = addSlot();
		weight = addSlot();
		licensePlate = addSlot();
		manuafacturingDate = addSlot();
		cost = addSlot();
		producer = addSlot();
		nameType = addSlot();
		so_yen = addSlot();
//		so_ban_dap = addSlot();
		so_ghe_sau = addSlot();
		deposit = addSlot();
	}
	
	public JLabel addSlot () {
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		JLabel label = new JLabel();
		add(label, c);
		return label;
	}
	
	@Override
	public void displayData() {
		id.setText("ID: " + t.getId());
		typeId.setText("Type ID: " + t.getTypeId());
		parkingId.setText("Parking ID: " + t.getParkingId());
		name.setText("Bike Name: " + t.getName());
		weight.setText("Weight: " + t.getWeight());
		licensePlate.setText("License Plate: " + t.getLicensePlate());
		manuafacturingDate.setText("Manuafacturing Date: " + t.getManuafacturingDate());
		cost.setText("Cost: " + t.getCost());
		producer.setText("Producer: " + t.getProducer());
		nameType.setText("Type Name: " + t.getNameType());
		so_yen.setText("Count Saddles: " + t.getSo_yen());
//		so_ban_dap.setText("Count Pendals: " + t.getSo_ban_dap());
		so_ghe_sau.setText("Count Seats: " + t.getSo_ghe_sau());
		deposit.setText("Deposit: " + t.getDeposit());
	}
}

