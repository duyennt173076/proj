package com.oms.components.abstractdata.controller;

public interface IDataCreateController<T> {
	public T create(T t);
}
