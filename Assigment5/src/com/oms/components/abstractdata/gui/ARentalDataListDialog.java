package com.oms.components.abstractdata.gui;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import com.oms.bean.Card;
import com.oms.bean.RentalData;
import com.oms.components.abstractdata.controller.RentalController;
import com.oms.serverapi.RentalApi;
import com.oms.serverapi.CardBankApi;

@SuppressWarnings("serial")
public abstract class ARentalDataListDialog<T> extends JDialog {
	protected List<T> t;
	protected GridBagLayout layout;
	protected GridBagConstraints c = new GridBagConstraints();
	private RentalData rentalData;
	private RentalController rentalController;

	List<JButton> listButton = new ArrayList<JButton>();

	public ARentalDataListDialog(List<T> t, RentalData rentalData) {
		super((Frame) null, "Thuê xe", true);
		this.t = t;
		this.rentalData = rentalData;
		this.setRentalController(new RentalController() {
			@Override
			public void rentalIndex(int i) {
				new CardBankApi().pay(getCardId(i), rentalData.getTien_coc());
				new RentalApi().rentalBike(rentalData, getCardId(i));
			}
			
		});
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);

		for (int i = 0; i < t.size(); i++) {
			this.buildControls(i);
			JButton saveButton = new JButton("Thuê");
			listButton.add(saveButton);
			saveButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int index = listButton.indexOf(saveButton);
					rentalController.rentalIndex(index);
					ARentalDataListDialog.this.dispose();
				}
			});
			c.gridx = 1;
			c.gridy = getLastRowIndex();
			getContentPane().add(saveButton, c);
		}

		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}

	protected int getLastRowIndex() {
		layout.layoutContainer(getContentPane());
		int[][] dim = layout.getLayoutDimensions();
		int rows = dim[1].length;
		return rows;
	}

	public abstract void buildControls(int i);

	public abstract T getNewData(int i);

	public abstract String getCardId(int i);

	public RentalController getRentalController() {
		return rentalController;
	}

	public void setRentalController(RentalController rentalController) {
		this.rentalController = rentalController;
	}
}
