package com.oms.components.cost.controller;




import com.oms.bean.Cost;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataAddPane;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.cost.gui.AdminCostListPane;
import com.oms.components.cost.gui.CostAddPane;


public abstract class AdminPPageController extends ADataPageController<Cost> {
	public AdminPPageController() {
		super();
	}
	
	@Override
	public ADataListPane<Cost> createListPane() {
		return new AdminCostListPane(this);
	}
	
	public abstract Cost updateCost(Cost cost);
	
	
	public ADataAddPane<Cost> createAddPane() {
		return new CostAddPane(this);
	}
}
