package com.oms.components.rental.controller;

import java.util.List;
import java.util.Map;

import com.oms.bean.RentalData;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.rental.gui.BikeSearchPane;
import com.oms.components.rental.gui.BikeSingelPane;
import com.oms.components.rental.gui.UserBikeListPane;
import com.oms.serverapi.RentalApi;



public class UserRentalPageController extends ADataPageController<RentalData> {
	public UserRentalPageController() {
		super();
	}
	
	@Override
	public List<? extends RentalData> search(Map<String, String> searchParams) {
		return new RentalApi().getRentalData(searchParams);
	}
	
	@Override
	public BikeSingelPane createSinglePane() {
		return new BikeSingelPane();
	}
	
	@Override
	public BikeSearchPane createSearchPane() {
		return new BikeSearchPane();
	}

	@Override
	public ADataListPane<RentalData> createListPane() {
		return new UserBikeListPane(this);
	}
	
}
