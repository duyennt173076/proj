package com.oms.components.rental.gui;

import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.RentalData;
import com.oms.bean.Card;
import com.oms.components.abstractdata.controller.RentalController;
import com.oms.components.abstractdata.gui.ARentalDataListDialog;
import com.oms.serverapi.RentalApi;
import com.oms.serverapi.CardBankApi;

@SuppressWarnings("serial")
public class CardListDialog extends ARentalDataListDialog<Card>{

	public CardListDialog(List<Card> card, RentalData rentalData) {
		super(card, rentalData);
	}

	@Override
	public void buildControls(int i) {
		int row = getLastRowIndex();
		JLabel idLabel = new JLabel("Mã thẻ: " + t.get(i).getId());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(idLabel, c);
		
		row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Tên chủ thẻ: " + t.get(i).getName());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		
		row = getLastRowIndex();
		JLabel moneyLabel = new JLabel("Số dư: " + t.get(i).getMoney());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(moneyLabel, c);
		
		row = getLastRowIndex();
		JLabel securityCodeLabel = new JLabel("Mã bảo mật: " + t.get(i).getSecurityCode());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(securityCodeLabel, c);
		
		row = getLastRowIndex();
		JLabel exprirationDateLabel = new JLabel("Ngày hết hạn: " + t.get(i).getExprirationDate());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(exprirationDateLabel, c);
	}

	@Override
	public Card getNewData(int i) {
		return t.get(i);
	}

	@Override
	public String getCardId(int i) {
		return t.get(i).getId();
	}

}

