package com.oms.components.biketype.ebiketype.gui;

import javax.swing.JLabel;

import com.oms.bean.EBikeType;
import com.oms.bean.BikeType;
import com.oms.components.biketype.gui.BikeTypeSinglePane;

@SuppressWarnings("serial")
public class EBikeTypeSinglePane extends BikeTypeSinglePane {
	private JLabel soGioSac;
	
	public EBikeTypeSinglePane() {
		super();
	}
	
	public EBikeTypeSinglePane(BikeType biketype) {
		this();
		this.t = biketype;

		displayData();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();

		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		soGioSac = new JLabel();
		add(soGioSac, c);
	}
	
	
	@Override
	public void displayData() {
		super.displayData();
		
		if (t instanceof EBikeType) {
			EBikeType ebiketype = (EBikeType) t;
			
			soGioSac.setText("So Gio Sac: " + ebiketype.getSo_gio_sac());
		}
	}
}
