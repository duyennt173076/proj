package com.oms.components.biketype.ebiketype.controller;

import java.util.List;
import java.util.Map;

import com.oms.bean.EBikeType;
import com.oms.bean.BikeType;
import com.oms.components.biketype.ebiketype.gui.EBikeTypeSearchPane;
import com.oms.components.biketype.ebiketype.gui.EBikeTypeSinglePane;
import com.oms.components.biketype.controller.AdminBikeTypePageController;
import com.oms.components.biketype.gui.BikeTypeSearchPane;
import com.oms.components.biketype.gui.BikeTypeSinglePane;
import com.oms.serverapi.BikeTypeApi;

public class AdminEBikeTypePageController extends AdminBikeTypePageController{
	@Override
	public List<? extends BikeType> search(Map<String, String> searchParams) {
		return new BikeTypeApi().getEBikeTypes(searchParams);
	}
	
	@Override
	public BikeTypeSinglePane createSinglePane() {
		return new EBikeTypeSinglePane();
	}
	
	@Override
	public BikeTypeSearchPane createSearchPane() {
		return new EBikeTypeSearchPane();
	}
	
	@Override
	public BikeType updateBikeType(BikeType biketype) {
		return new BikeTypeApi().updateEBikeType((EBikeType) biketype);
	}
	@Override
	public BikeType addBikeType(BikeType biketype) {
		return new BikeTypeApi().addEBikeType((EBikeType) biketype);
	}
	@Override
	public BikeType removeBikeType(BikeType biketype) {
		return new BikeTypeApi().removeEBikeType((EBikeType) biketype);
	}
}
