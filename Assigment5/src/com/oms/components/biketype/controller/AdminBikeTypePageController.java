package com.oms.components.biketype.controller;

import com.oms.bean.BikeType;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.biketype.gui.AdminBikeTypeListPane;

public abstract class AdminBikeTypePageController extends ADataPageController<BikeType> {
	public AdminBikeTypePageController() {
		super();
	}
	
	@Override
	public ADataListPane<BikeType> createListPane() {
		return new AdminBikeTypeListPane(this);
	}
	
	public abstract BikeType updateBikeType(BikeType biketype);
	public abstract BikeType addBikeType(BikeType biketype);
	public abstract BikeType removeBikeType(BikeType biketype);
	
}
