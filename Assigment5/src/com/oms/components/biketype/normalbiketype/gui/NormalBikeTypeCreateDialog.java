package com.oms.components.biketype.normalbiketype.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.BikeType;
import com.oms.bean.NormalBikeType;
import com.oms.components.abstractdata.controller.IDataCreateController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.biketype.gui.BikeTypeCreateDialog;

@SuppressWarnings("serial")
public class NormalBikeTypeCreateDialog extends BikeTypeCreateDialog{
	
	
	public NormalBikeTypeCreateDialog(BikeType biketype, IDataManageController<BikeType> controller) {
		super(biketype, controller);
	}

	@Override
	public void buildControls() {
		super.buildControls();
	}
	
	@Override
	public BikeType getNewData() {
		super.getNewData();
		return t;
	}
}