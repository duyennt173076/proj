package com.oms.components.biketype.normalbiketype.gui;

import javax.swing.JLabel;

import com.oms.bean.NormalBikeType;
import com.oms.bean.BikeType;
import com.oms.components.biketype.gui.BikeTypeSinglePane;

@SuppressWarnings("serial")
public class NormalBikeTypeSinglePane extends BikeTypeSinglePane {
	
	public NormalBikeTypeSinglePane() {
		super();
	}
	
	public NormalBikeTypeSinglePane(BikeType biketype) {
		this();
		this.t = biketype;

		displayData();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();

	}
	
	
	@Override
	public void displayData() {
		super.displayData();
	}
}
