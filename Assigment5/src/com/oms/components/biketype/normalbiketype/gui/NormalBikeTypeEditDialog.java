package com.oms.components.biketype.normalbiketype.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.NormalBikeType;
import com.oms.bean.BikeType;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.biketype.gui.BikeTypeEditDialog;

@SuppressWarnings("serial")
public class NormalBikeTypeEditDialog extends BikeTypeEditDialog{
	
	public NormalBikeTypeEditDialog(BikeType biketype, IDataManageController<BikeType> controller) {
		super(biketype, controller);
	}

	@Override
	public void buildControls() {
		super.buildControls();
	}

	@Override
	public BikeType getNewData() {
		super.getNewData();
		
		return t;
	}
}
