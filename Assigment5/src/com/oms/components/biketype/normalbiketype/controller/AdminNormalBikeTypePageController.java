package com.oms.components.biketype.normalbiketype.controller;

import java.util.List;
import java.util.Map;

import com.oms.bean.NormalBikeType;
import com.oms.bean.BikeType;
import com.oms.bean.EBikeType;
import com.oms.components.biketype.normalbiketype.gui.NormalBikeTypeSearchPane;
import com.oms.components.biketype.normalbiketype.gui.NormalBikeTypeSinglePane;
import com.oms.components.biketype.controller.AdminBikeTypePageController;
import com.oms.components.biketype.gui.BikeTypeSearchPane;
import com.oms.components.biketype.gui.BikeTypeSinglePane;
import com.oms.serverapi.BikeTypeApi;

public class AdminNormalBikeTypePageController extends AdminBikeTypePageController{
	@Override
	public List<? extends BikeType> search(Map<String, String> searchParams) {
		return new BikeTypeApi().getNormalBikeTypes(searchParams);
	}
	
	@Override
	public BikeTypeSinglePane createSinglePane() {
		return new NormalBikeTypeSinglePane();
	}
	
	@Override
	public BikeTypeSearchPane createSearchPane() {
		return new NormalBikeTypeSearchPane();
	}
	
	@Override
	public BikeType updateBikeType(BikeType biketype) {
		return new BikeTypeApi().updateNormalBikeType((NormalBikeType) biketype);
	}
	@Override
	public BikeType addBikeType(BikeType biketype) {
		return new BikeTypeApi().addNormalBikeType((NormalBikeType) biketype);
	}
	@Override
	public BikeType removeBikeType(BikeType biketype) {
		return new BikeTypeApi().removeNormalBikeType((NormalBikeType) biketype);
	}
}
