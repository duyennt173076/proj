package com.oms.components.biketype.gui;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.components.abstractdata.gui.ADataSearchPane;

@SuppressWarnings("serial")
public class BikeTypeSearchPane extends ADataSearchPane {
	private JTextField nameField;
	private JTextField tienCocField;
	private JTextField mucGiaField;

	public BikeTypeSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		nameField = addSlot("Name");
	}

	public JTextField addSlot(String labelTitle) {
		JLabel label = new JLabel(labelTitle);
		JTextField field = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(label, c);
		c.gridx = 1;
		c.gridy = row;
		add(field, c);
		return field;
	}

	
	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		if (!nameField.getText().trim().equals("")) {
			res.put("name", nameField.getText().trim());
		}	
		return res;
	}
}
