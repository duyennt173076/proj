package com.oms.components.biketype.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.BikeType;
import com.oms.components.abstractdata.controller.IDataCreateController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataCreateDialog;

@SuppressWarnings("serial")
public class BikeTypeCreateDialog extends ADataCreateDialog<BikeType>{
	
	private JTextField id;
	private JTextField name;
	private JTextField so_yen;
	private JTextField tien_coc;
	private JTextField so_ghe_sau;
	private JTextField muc_gia;
	
	public BikeTypeCreateDialog(BikeType biketype, IDataManageController<BikeType> controller) {
		super(biketype, controller);
	}

	@Override
	public void buildControls() {
		id = addSlot("ID", t.getId());
		name = addSlot("Name", t.getName());
		so_yen = addSlot("So Yen", Integer.toString(t.getSo_yen()));
		tien_coc = addSlot("Tien Coc", Integer.toString(t.getTien_coc()));
		so_ghe_sau = addSlot("So Ghe Sau", Integer.toString(t.getSo_ghe_sau()));
		muc_gia = addSlot("Muc gia", Float.toString(t.getMuc_gia()));
	}

	public JTextField addSlot(String labelTitle, String text) {
		int row = getLastRowIndex();
		JLabel label = new JLabel(labelTitle);
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(label, c);
		JTextField field = new JTextField(15);
		field.setText(text);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(field, c);
		return field;
	}
	
	@Override
	public BikeType getNewData() {
		String w;
		w = id.getText().trim();
		if (!w.equals("")) t.setId(w);
		w = name.getText().trim();
		if (!w.equals("")) t.setName(w);
		w = so_yen.getText().trim();
		if (!w.equals("")) t.setSo_yen(Integer.parseInt(w)); else t.setSo_yen((int)0);
		w = tien_coc.getText().trim();
		if (!w.equals("")) t.setTien_coc(Integer.parseInt(w)); else t.setTien_coc((int)0);
		w = so_ghe_sau.getText().trim();
		if (!w.equals("")) t.setSo_ghe_sau(Integer.parseInt(w)); else t.setSo_ghe_sau((int)0);
		w = muc_gia.getText().trim();
		if (!w.equals("")) t.setMuc_gia(Float.parseFloat(w)); else t.setMuc_gia((float)0);
		
		return t;
	}
}

