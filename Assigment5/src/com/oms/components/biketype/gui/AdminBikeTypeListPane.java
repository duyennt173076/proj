package com.oms.components.biketype.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.oms.bean.BikeType;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.abstractdata.gui.ADataSinglePane;
import com.oms.components.bike.gui.AdminBikeListPane;
import com.oms.components.biketype.ebiketype.gui.EBikeTypeEditDialog;
import com.oms.components.biketype.ebiketype.gui.EBikeTypeCreateDialog;
import com.oms.components.biketype.normalbiketype.gui.NormalBikeTypeEditDialog;
import com.oms.components.biketype.normalbiketype.gui.NormalBikeTypeCreateDialog;
import com.oms.components.biketype.controller.AdminBikeTypePageController;
import com.oms.components.biketype.ebiketype.controller.AdminEBikeTypePageController;
import com.oms.components.biketype.normalbiketype.controller.AdminNormalBikeTypePageController;

@SuppressWarnings("serial")
public class AdminBikeTypeListPane extends ADataListPane<BikeType>{
	
	public AdminBikeTypeListPane(ADataPageController<BikeType> controller) {
		this.controller = controller;
	}
	
	@Override
	public void decorateSinglePane(ADataSinglePane<BikeType> singlePane) {
		JButton button = new JButton("Edit");
		singlePane.addDataHandlingComponent(button);
		
		JButton button1 = new JButton("Remove");
		singlePane.addDataHandlingComponent(button1);
		
		JButton button2 = new JButton("Add");
		singlePane.addDataHandlingComponent(button2);
		
		AdminBikeTypeListPane self = this;
		
		IDataManageController<BikeType> manageController = new IDataManageController<BikeType>() {
			@Override
			public void update(BikeType t) {
				if (controller instanceof AdminBikeTypePageController) {
					BikeType newBikeType = ((AdminBikeTypePageController) controller).updateBikeType(t);
					singlePane.updateData(newBikeType);
				}
			}

			@Override
			public void create(BikeType t) {
				if (controller instanceof AdminBikeTypePageController) {
					((AdminBikeTypePageController) controller).addBikeType(t);
				}
			}

			@Override
			public void read(BikeType t) {
			}

			@Override
			public void delete(BikeType t) {
				if (controller instanceof AdminBikeTypePageController) {
					((AdminBikeTypePageController) controller).removeBikeType(t);
				}
			}
		};
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(controller instanceof AdminNormalBikeTypePageController) {
				new NormalBikeTypeEditDialog(singlePane.getData(), manageController);
				}
				if(controller instanceof AdminEBikeTypePageController) {
					new EBikeTypeEditDialog(singlePane.getData(), manageController);
					}
			}
		});
		
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(controller instanceof AdminNormalBikeTypePageController) {
					manageController.delete(singlePane.getData());
					self.handleDeleteSinglePane(singlePane.getData());
				}
				if(controller instanceof AdminEBikeTypePageController) {
					manageController.delete(singlePane.getData());
					self.handleDeleteSinglePane(singlePane.getData());
				}
			}
		});
		
		button2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(controller instanceof AdminNormalBikeTypePageController) {
				new NormalBikeTypeCreateDialog(singlePane.getData(), manageController);
				}
				if(controller instanceof AdminEBikeTypePageController) {
					new EBikeTypeCreateDialog(singlePane.getData(), manageController);
					}
			}
		});
	}
	public void handleDeleteSinglePane(BikeType biketype) {
		list.remove(biketype);
		this.updateData(this.list);
	}
}
