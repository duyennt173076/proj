package com.oms.components.biketype.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.BikeType;
import com.oms.components.abstractdata.gui.ADataSinglePane;

@SuppressWarnings("serial")
public class BikeTypeSinglePane extends ADataSinglePane<BikeType>{
	private JLabel name;
	private JLabel soYen;
	private JLabel tienCoc;
	private JLabel soGheSau;
	private JLabel mucGia;
	
	public BikeTypeSinglePane() {
		super();
	}
		
	
	public BikeTypeSinglePane(BikeType biketype) {
		this();
		this.t = biketype;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		name = addSlot();
		soYen = addSlot();
		tienCoc = addSlot();
		soGheSau = addSlot();
		mucGia = addSlot();
		
	}
	
	public JLabel addSlot () {
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		JLabel label = new JLabel();
		add(label, c);
		return label;
	}
	
	@Override
	public void displayData() {
		name.setText("Name: " + t.getName());
		soYen.setText("So Yen: " + t.getSo_yen() + "");
		tienCoc.setText("Tien Coc: " + t.getTien_coc() + "");
		soGheSau.setText("So Ghe Sau: " + t.getSo_ghe_sau() + "");
		mucGia.setText("Muc Gia: " + t.getMuc_gia() + "");
	}
}
