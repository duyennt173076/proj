package com.oms.components.card.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.Card;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataEditDialog;

@SuppressWarnings("serial")
public class CardEditDialog extends ADataEditDialog<Card>{
	
	private JTextField nameField;
	private JTextField securityCodeField;
	private JTextField exprirationDateField;
	
	public CardEditDialog(Card card, IDataManageController<Card> controller) {
		super(card, controller);
	}

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Tên chủ thẻ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);
		nameField.setText(t.getName());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);
		
		
		row = getLastRowIndex();
		JLabel securityCodeLabel = new JLabel("Mã bảo mật");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(securityCodeLabel, c);
		securityCodeField = new JTextField(15);
		securityCodeField.setText(t.getSecurityCode());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(securityCodeField, c);

		row = getLastRowIndex();
		JLabel exprirationDateLabel = new JLabel("Ngày kết thúc");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(exprirationDateLabel, c);
		exprirationDateField = new JTextField(15);
		exprirationDateField.setText(t.getExprirationDate());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(exprirationDateField, c);
		
		
	}

	@Override
	public Card getNewData() {
		t.setName(nameField.getText());
		t.setSecurityCode(securityCodeField.getText());
		t.setExprirationDate(exprirationDateField.getText());
		
		return t;
	}
}

