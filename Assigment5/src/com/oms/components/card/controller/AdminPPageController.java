package com.oms.components.card.controller;




import com.oms.bean.Card;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataAddPane;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.card.gui.UserCardListPane;
import com.oms.components.card.gui.CardAddPane;


public abstract class AdminPPageController extends ADataPageController<Card> {
	public AdminPPageController() {
		super();
	}
	
	@Override
	public ADataListPane<Card> createListPane() {
		return new UserCardListPane(this);
	}
	
	public abstract Card updateCard(Card card);
	
	public ADataAddPane<Card> createAddPane() {
		return new CardAddPane(this);
	}
}
