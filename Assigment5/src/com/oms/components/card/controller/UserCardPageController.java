package com.oms.components.card.controller;

import java.util.List;
import java.util.Map;

import com.oms.bean.Card;
import com.oms.components.card.gui.CardSearchPane;
import com.oms.components.card.gui.CardSingelPane;
import com.oms.serverapi.AddCardBankApi;
import com.oms.serverapi.CardApi;



public class UserCardPageController extends AdminPPageController{
	@Override
	public List<? extends Card> search(Map<String, String> searchParams) {
		return new CardApi().getCard(searchParams);
	}
	
	@Override
	public CardSingelPane createSinglePane() {
		return new CardSingelPane();
	}
	
	@Override
	public CardSearchPane createSearchPane() {
		return new CardSearchPane();
	}
	
	@Override
	public Card updateCard(Card card) {
		return new CardApi().updateCard((Card) card);
	}

	public Card addCard(Card card) {
		Card c = new AddCardBankApi().match((Card)card);
		if (c!= null)new CardApi().addCard((Card) c);
		return c;
	}

	public Card deleteCard(Card card) {
		return new CardApi().deleteCard((Card) card);
	}

}
