package com.oms.app.admin;

import javax.swing.JPanel;
import com.oms.bean.Parking;
import com.oms.bean.BikeType;
import com.oms.bean.bike.Bike;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.bike.controller.AdminBikePageController;
import com.oms.components.bike.ebike.AdminEBikePageController;
import com.oms.components.parking.controller.AdminParkingPageController;
import com.oms.components.biketype.ebiketype.controller.AdminEBikeTypePageController;
import com.oms.components.biketype.normalbiketype.controller.AdminNormalBikeTypePageController;

import com.oms.components.cost.controller.AdminCostPageController;
import com.oms.bean.Cost;

public class OMSAdminController {
	public OMSAdminController() {
	}
	
	public JPanel getParkingPage() {
		ADataPageController<Parking> controller = new AdminParkingPageController();
		return controller.getDataPagePane();
	}
	
	public JPanel getBikePage() {
		ADataPageController<Bike> controller = new AdminBikePageController();
		return controller.getDataPagePane();
	}
	
	public JPanel getEBikePage() {
		ADataPageController<Bike> controller = new AdminEBikePageController();
		return controller.getDataPagePane();
	}
	
	public JPanel getNormalBikeTypePage() {
		ADataPageController<BikeType> controller = new AdminNormalBikeTypePageController();
		return controller.getDataPagePane();
	}
	public JPanel getEBikeTypePage() {
		ADataPageController<BikeType> controller = new AdminEBikeTypePageController();
		return controller.getDataPagePane();
	}
	
	//
	public JPanel getCostPage() {
		ADataPageController<Cost> controller = new AdminCostPageController();
		return controller.getDataPagePane();
	}
}