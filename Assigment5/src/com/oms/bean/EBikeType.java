package com.oms.bean;



public class EBikeType extends BikeType {
	private float so_gio_sac;
	
	public EBikeType() {
		super();
	}
	
	public EBikeType(String id, String name, int so_yen, int tien_coc, int so_ghe_sau, float muc_gia) {
		super(id, name, so_yen, tien_coc, so_ghe_sau, muc_gia);
	}


	public EBikeType(String id, String name, int so_yen, int tien_coc, int so_ghe_sau, float muc_gia, float so_gio_sac) {
		super(id, name, so_yen, tien_coc, so_ghe_sau, muc_gia);
		this.so_gio_sac = so_gio_sac;
	}

	public float getSo_gio_sac() {
		return so_gio_sac;
	}

	public void setSo_gio_sac(float so_gio_sac) {
		this.so_gio_sac = so_gio_sac;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", so gio sac: " + so_gio_sac;
	}
	
	@Override
	public boolean match(BikeType bike) {
		if (bike == null)
			return true;
		
		
		boolean res = super.match(bike);
		if (!res) {
			return false;
		}
		
		
		if (!(bike instanceof EBikeType))
			return false;
		EBikeType ebike= (EBikeType) bike;
		
		if (ebike.so_gio_sac != 0 && this.so_gio_sac != ebike.so_gio_sac) {
			return false;
		}
		return true;
	}
}