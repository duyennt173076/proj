package com.oms.bean;



public class NormalBikeType extends BikeType {
	
	public NormalBikeType() {
		super();
	}
	
	public NormalBikeType(String id, String name, int so_yen, int tien_coc, int so_ghe_sau, float muc_gia) {
		super(id, name, so_yen, tien_coc, so_ghe_sau, muc_gia);
	}

	
	@Override
	public String toString() {
		return super.toString();
	}
	
	@Override
	public boolean match(BikeType bike) {
		if (bike == null)
			return true;
		
		
		boolean res = super.match(bike);
		if (!res) {
			return false;
		}
		
		
		if (!(bike instanceof NormalBikeType))
			return false;

		return true;
	}
}